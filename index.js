const request = require('request');

const Web3 = require('web3');
const web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:8591'));

let heightBTC;
let heightETH;
let heightLTC;
let heightDASH;

let getHeightBTC = () =>  {
    request('https://api.blockcypher.com/v1/btc/main', function (error, response, body) {
        if ( error == null && response.statusCode === 200) {
            heightBTC =  JSON.parse(body).height;
            console.log('Height BTC: ', heightBTC);
        } else {
            console.log('ERROR', error, response);
        }
    });
};

let getHeightLTC = () =>  {
    request('https://api.blockcypher.com/v1/ltc/main', function (error, response, body) {
        if ( error == null && response.statusCode === 200) {
            heightLTC =  JSON.parse(body).height;
            console.log('Height LTC: ', heightLTC);
        } else {
            console.log('ERROR', error, response);
        }
    });
};

let getHeightDASH = () =>  {
    request('https://api.blockcypher.com/v1/dash/main', function (error, response, body) {
        if ( error == null && response.statusCode === 200) {
            heightDASH =  JSON.parse(body).height;
            console.log('Height DASH: ', heightDASH);
        } else {
            console.log('ERROR', error, response);
        }
    });
};

let getHeightETH = () =>  {
    request('https://api.infura.io/v1/jsonrpc/ropsten/eth_blockNumber', function (error, response, body) {
        if ( error == null && response.statusCode === 200) {
            heightETH =  JSON.parse(body).result;
            console.log('Height ETH: ', parseInt(heightETH, 16));
        } else {
            console.log('ERROR', error, response);
        }
    });
};

let OurHighestBlockETH = () => {
    web3.eth.getBlockNumber().then(console.log);
};

let CheckBTC = setInterval(getHeightBTC, 30000);
let CheckLTC = setInterval(getHeightLTC, 30000);
let CheckDASH = setInterval(getHeightDASH, 30000);
let CheckETH = setInterval(getHeightETH, 30000);
let CheckOurETH = setInterval(OurHighestBlockETH, 30000)


/*async function f() {
    const sync = await web3.eth.isSyncing();
    const peerCount = await web3.eth.net.getPeerCount();
    const blockNumber = await web3.eth.getBlockNumber();
    console.log(sync, peerCount, blockNumber);
}
f();*/
